import UIKit

var str = "Hello, playground"

//使用 enum 关键词来创建枚举并且把它们的整个定义放在一对大括号内：
enum SomeEnumeration{
    
}

//下面是用枚举表示指南针四个方向的例子：
enum CompassPoint{
    case north
    case south
    case east
    case west
}

//与 C 和 Objective-C 不同，Swift 的枚举成员在被创建时不会被赋予一个默认的整型值。在上面的 CompassPoint 例子中，north，south，east 和 west 不会被隐式地赋值为 0，1，2 和 3。相反，这些枚举成员本身就是完备的值，这些值的类型是已经明确定义好的 CompassPoint 类型。


print(CompassPoint.north)

//使用
//directionToHome的类型可以根据值推断出来
var directionToHome = CompassPoint.south

//当directionToHome类型已知时,再次赋值时可以省略枚举类型名
directionToHome = .east

print(directionToHome)

//使用 Switch 语句匹配枚举值
switch directionToHome {
    case .north:
        print("北方")
    case .south:
        print("南方")
    case .east:
        print("东方")
    case .west:
        print("西方")
    
}

//枚举成员的遍历
//for direct in CompassPoint{
//    print(direct)
//}
//测试不通过,可能是swift5去掉了这个语法

