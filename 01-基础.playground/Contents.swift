//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

//输出常量和变量
let friendlyWelcome = "hello"
print("the current value of friendlyWelcome is \(friendlyWelcome)")

//整数类型范围(min,max)
let minValue = Int8.min;
let maxValue = Int8.max;
let minValue1 = Int.min;
let maxValue2 = Int.max;

print(minValue)
print(maxValue)
print(minValue1)
print(maxValue2)

//类型转换
let twoThousand: UInt16 = 2000;
let one:UInt8 = 1;
//不同类型的基本类型不能直接运算,需要转换成相同类型之后再进行运算
let twoThousandAndOne = twoThousand + UInt16(one);

//类型别名 typealias
typealias shortInt = Int8;
let shortIntMin = shortInt.min;

//元组 把多个值组合成一个复合值。元组内的值可以是任意类型，并不要求是相同类型。
let http404Error = (404,"Not Found")
print(http404Error)

//可以把一个元组分解成单独的常量或变量,就可以正常使用
let (statusCode,statusMsg) = http404Error
print(statusCode)
print(statusMsg)

//如果只需要元组中一部分数据,可以使用_忽略掉不需要的值
let (statusCode1,_) = http404Error
print(statusCode1)

//可以使用下标来访问元组中的元素,下标从0开始
let statusMsg1 = http404Error.1
print(statusMsg1)

//定义元组时,可以给单个元素命名
let http200Status = (statusCode2:200,msg2:"OK")
//然后可以使用元组中的元素名来访问
print(http200Status.statusCode2)
print(http200Status.msg2)

//可选类型
var option1:Int?
option1 = 2;

//强制解析:在可选类型变量后写一个!,表示可选性有值,并取出该值
if option1 != nil {
    print(option1!)
}

//可选绑定
//使用可选绑定（optional binding）来判断可选类型是否包含值，如果包含就把值赋给一个临时常量或者变量。可选绑定可以用在 if 和 while 语句中，这条语句不仅可以用来判断可选类型中是否有值，同时可以将可选类型中的值赋给一个常量或者变量

if let number1 = option1 {
    print(number1)
}else{
    print("可选值为nil")
}

let age = 3;
assert(age >= 0, "年龄不可以为负数")

if age < 0 {
    print("年龄小于0");
}else{
//    assertionFailure("年龄小于0")
}
