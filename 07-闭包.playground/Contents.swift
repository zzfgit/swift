import UIKit

var str = "Hello, playground"

let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]

func backwark(_ s1:String, _ s2:String) -> Bool {
    return s1 > s2;
}

var reversedNames = names.sorted(by: backwark(_:_:));
print(reversedNames)

reversedNames  = names.sorted { (s1:String, s2:String) -> Bool in
    return s1 > s2;
}
print(reversedNames)

//根据上下文推断类型
//闭包中的参数类型,和返回值类型可以根据上下文推断,所以可以省略
reversedNames = names.sorted { (s1, s2) in
    return s1 > s2;
}
print(reversedNames)

//单表达式闭包的隐式返回
//单行表达式闭包可以通过省略 return 关键字来隐式返回单行表达式的结果，如上版本的例子可以改写为：
reversedNames = names.sorted(by: { s1, s2 in s1 > s2})
print(reversedNames)

//参数名称缩写
//Swift 自动为内联闭包提供了参数名称缩写功能，你可以直接通过 $0，$1，$2 来顺序调用闭包的参数，以此类推。
//
//如果你在闭包表达式中使用参数名称缩写，你可以在闭包定义中省略参数列表，并且对应参数名称缩写的类型会通过函数类型进行推断。in 关键字也同样可以被省略，因为此时闭包表达式完全由闭包函数体构成：
reversedNames = names.sorted(by: {$0 > $1})
print(reversedNames)
//在这个例子中，$0 和 $1 表示闭包中第一个和第二个 String 类型的参数。

//运算符方法
//实际上还有一种更简短的方式来编写上面例子中的闭包表达式。Swift 的 String 类型定义了关于大于号（>）的字符串实现，其作为一个函数接受两个 String 类型的参数并返回 Bool 类型的值。而这正好与 sorted(by:) 方法的参数需要的函数类型相符合。因此，你可以简单地传递一个大于号，Swift 可以自动推断找到系统自带的那个字符串函数的实现：
reversedNames = names.sorted(by: >)
print(reversedNames)


//尾随闭包
//如果你需要将一个很长的闭包表达式作为最后一个参数传递给函数，将这个闭包替换成为尾随闭包的形式很有用。尾随闭包是一个书写在函数圆括号之后的闭包表达式，函数支持将其作为最后一个参数调用。在使用尾随闭包时，你不用写出它的参数标签：
reversedNames = names.sorted(){$0 > $1}
print(reversedNames)
