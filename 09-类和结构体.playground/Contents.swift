import UIKit

var str = "Hello, playground"

//类和结构体
//类型定义的语法

//结构体和类有着相似的定义方式。你通过 struct 关键字引入结构体，通过 class 关键字引入类，并将它们的具体定义放在一对大括号中：
struct SomeStructure{
    //在里边定义结构体的内容
}

class SomeClass{
    //在里边定义类的内容
}

//定义结构体和定义类的示例：
struct Resolution{
    var width = 0
    var height = 0
}

class VideoMode{
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name:String?
}

//创建一个结构体和类的实例
let someResolution = Resolution()
let someVideoMode = VideoMode()

//属性访问
print(someResolution.width)
print(someResolution.height)
print(someVideoMode.resolution)

//属性赋值
someVideoMode.resolution.width = 100;
print(someVideoMode.resolution.width)

//结构体类型的成员逐一构造器
//所有结构体都有一个自动生成的成员逐一构造器，用于初始化新结构体实例中成员的属性。新实例中各个属性的初始值可以通过属性的名称传递到成员逐一构造器之中：
let vga = Resolution(width: 640, height: 480)
print("vga:\(vga)")

//与结构体不同，类实例没有默认的成员逐一构造器。构造过程 章节会对构造器进行更详细的讨论。

//结构体和枚举是值类型
//值类型在传递的时候都会被复制

var hd = Resolution(width: 1920, height: 1080)
var cinema = hd
//将hd赋值给cinema,两个结构体变量的值一样,但是是两个不同的实例

print(hd)
print(cinema)

//改变cinema的属性值,不影响hd的属性值
cinema.width = 2048
print(hd)
print(cinema)


//类是引用类型
//与值类型不同，引用类型在被赋予到一个变量、常量或者被传递到一个函数时，其值不会被拷贝。因此，使用的是已存在实例的引用，而不是其拷贝。
class Person{
    var name:String?
    var age:Int?
}

let xiaoming = Person()
xiaoming.name = "小明"
xiaoming.age = 13
print(xiaoming.name!)//小明

let xiaoming2 = xiaoming;
xiaoming2.name = "小明2"
print(xiaoming.name!)//小明2
//修改了xiaoming2的值,xiaoming的值也会跟着修改,因为他们两个指向的是一个实例对象

//恒等运算符
//判定两个常量或者变量是否引用同一个类实例有时很有用。为了达到这个目的，Swift 提供了两个恒等运算符：
                    //相同（===）
                    //不相同（!==）

if xiaoming2 === xiaoming {
    print("小明2全等于小明")
}else{
    print("小明2不全等于小明")
}
//请注意，“相同”（用三个等号表示，===）与“等于”（用两个等号表示，==）的不同。“相同”表示两个类类型（class type）的常量或者变量引用同一个类实例。“等于”表示两个实例的值“相等”或“等价”，判定时要遵照设计者定义的评判标准。

 
