//: A UIKit based Playground for presenting user interface
//基本运算符
import UIKit
import PlaygroundSupport

//元组比较
let tuple1 = (1,"zebra")
let tuple2 = (2,"apple")

if tuple1 < tuple2 {
    print("tuple1小于tuple2")
}

//元组中的元素可以单独比较
if tuple1.0 < tuple2.0 {
    print("元组1的第一个元素:\(tuple1.0)小于元组2的第一个元素:\(tuple2.0)")
}

//注意:比较运算符不可以比较布尔值
//所以:包含布尔值的元组不可以进行比较
//例如:下面代码报错:Binary operator '<' cannot be applied to two '(Int, Bool)' operands
//if (1,true) < (2,false) {
//
//}

//空合运算符
//空合运算符（a ?? b）将对可选类型 a 进行空判断，如果 a 包含一个值就进行解包，否则就返回一个默认值 b。表达式 a 必须是 Optional 类型。默认值 b 的类型必须要和 a 存储值的类型保持一致。
let defaultColorName = "red"
var userDefinedColorName: String? //默认为nil

//判断第一个可选项是否有值,如果有值,返回可选项的值,如果没有值,返回第二个值
var colorNameToUse = userDefinedColorName ?? defaultColorName
print(colorNameToUse)

//区间运算符
//闭区间运算符 a...b 从a到b,包含a和b,b必须大于a
for index in 1...5 {
    print("\(index) * 5 = \(index * 5)")
}

//半开区间运算符 a..<b  从 a 到 b 但不包括 b 的区间
let names = ["Anna","Alex","Brian","Jack"]
let count = names.count
for i in 0..<count {
    print("第 \(i + 1) 个人叫 \(names[i])")
}


