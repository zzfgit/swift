import UIKit

var str = "Hello, playground"
//数组（Arrays）
//数组使用有序列表存储同一类型的多个值。相同的值可以多次出现在一个数组的不同位置中。

//创建一个空数组
var someInts = [Int]()
print("someInts if of type [Int] with \(someInts)")

someInts.append(3);
print("someInts if of type [Int] with \(someInts)")

someInts = []
print("someInts if of type [Int] with \(someInts)")

//创建一个带有默认值的数组
//传入元素默认值和元素个数
var threeDoubles = Array(repeating: 0.0, count: 3);
print(threeDoubles)

threeDoubles.append(0.3);
print(threeDoubles)

//数组元素相同的数组可以使用 + 拼接成为一个新的数组

//使用数组字面量构造数组
var shoppinglist:[String] = ["Eggs","Milk"];
print(shoppinglist)

//访问和修改数组
//我们可以通过数组的方法和属性来访问和修改数组，或者使用下标语法。
print(shoppinglist.count)
print(shoppinglist[1])

shoppinglist[1] = "banana"
print(shoppinglist)

//插入数据:在指定索引之前插入数据
shoppinglist.insert("tomatoes", at: 1);
print(shoppinglist)

//移除指定索引处的数据并返回
shoppinglist.remove(at: 1);
print(shoppinglist)

//移除最后一个元素
shoppinglist.removeLast()
print(shoppinglist)

shoppinglist.append("tomatoes")
shoppinglist.append("banana")
print(shoppinglist)

print()
//数组的遍历 for-in
for item in shoppinglist {
    print(item)
}

//如果我们同时需要每个数据项的值和索引值，可以使用 enumerated() 方法来进行数组遍历。enumerated() 返回一个由每一个数据项索引值和数据值组成的元组。我们可以把这个元组分解成临时常量或者变量来进行遍历：
for (index,value) in shoppinglist.enumerated() {
    print(index,value)
}

//集合
//集合中的元素不可重复

//创建一个空的集合
var letters = Set<Character>();
print(letters)

letters.insert("a")
print(letters)

//用数组字面量创建集合
var favoriteGenres:Set<String> = ["Rock","Classical","Hip hop"];

//集合基本操作
let oddSet:Set = [1,3,5,7,9]
let evenSet:Set = [0,2,4,6,8]
let lessThan5Set:Set = [0,1,2,3,4]

//合并两个集合
let oddAndEvenSet:Set = oddSet.union(evenSet);
print(oddAndEvenSet.sorted())

//根据两个集合的交集创建新的集合
let intersectionSet:Set = oddSet.intersection(lessThan5Set)
print(intersectionSet)

//根据两个集合不相交的元素创建新的集合
let differenceSet:Set = oddSet.symmetricDifference(lessThan5Set);
print(differenceSet.sorted())

//排除一个集合中和另一个集合的相交元素创建一个新集合
let subtractionSet = oddSet.subtracting(lessThan5Set);
print(subtractionSet)

//字典
//创建一个空字典
//使用构造语法,创建一个拥有特定类型的空字典
var namesOfIntegers = [Int:String]();
print(namesOfIntegers)

//通过字面量类创建爱你一个空字典
var airportsDict: [String:String] = ["a":"dog","b":"cat","c":"pig"]
print(airportsDict)

//使用字面量创建字典时,如果键和值都有各自一致的类型,那么就不必写出字典的类型,swift会自动推断
var airportsDict2 = ["a":"dog","b":"cat","c":"pig"]
print(airportsDict2)


//字典的元素数量:count
print(airportsDict2.count)

//判断字典count是否为0
print(airportsDict2.isEmpty)

//为字典添加元素
airportsDict2["d"] = "monky"
print(airportsDict2)

//修改制定key的值
airportsDict2["d"] = "horse"
print(airportsDict2)

//修改指定key的值:使用updateValue(_:forKey:)
airportsDict2.updateValue("cow", forKey: "d")
print(airportsDict2)

// * updateValue(_:forKey:)这个方法返回一个可选值
//   如果更新的key原来有值,就返回原来的旧值,否则返回nil
if let oldValue = airportsDict2.updateValue("bird", forKey: "d") {
    print("oldValue is \(oldValue)")
    print("newValue is \(airportsDict2["d"]!)")
}

//可以通过给某个key对应的value赋值为nil,从字典中移除一个键值对
airportsDict2["d"] = nil
print(airportsDict2)

//移除键值对:使用removeValue(forKey:)方法
// * 如果要移除的键值对存在,那么这个方法返回旧值,如果旧值不存在,那么这个方法返回nil
airportsDict2.removeValue(forKey: "c")
print(airportsDict2)

if let oldValueOfc = airportsDict2.removeValue(forKey: "c") {
    print("the oldVelueOfc is \(oldValueOfc)")
}else{
    print("the oldValueOfc is nil")
}

//字典遍历
//可以使用for-in方法遍历字典,会返回(key,value)形式的元组数据
for (key,value) in airportsDict2 {
    print("key:\(key) --- value:\(value)")
}

//访问字典的keys属性,遍历字典的键
for key in airportsDict2.keys {
    print("key:\(key)")
}

//访问字典的values属性,遍历字典的值
for value in airportsDict2.values {
    print("value:\(value)")
}
