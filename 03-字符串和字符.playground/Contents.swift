import UIKit

var str = "Hello, playground"

//多行字符串字面量
//如果你需要一个字符串是跨越多行的，那就使用多行字符串字面量 — 由一对三个双引号包裹着的具有固定顺序的文本字符集：

let quotation = """
 The White Rabbit put on his spectacles.  "Where shall I begin,
please your Majesty?" he asked.

"Begin at the beginning," the King said gravely, "and go on
till you come to the end; then stop."
"""
print(quotation)

//扩展字符串分隔符
//您可以将字符串文字放在扩展分隔符中，这样字符串中的特殊字符将会被直接包含而非转义后的效果。将字符串放在引号（"）中并用数字符号（＃）括起来。例如，打印字符串文字 ＃"Line 1 \ nLine 2"＃ 打印换行符转义序列（\n）而不是进行换行打印。
let str1 = #"LIne 1 \nLine 2"#//使用##包含的字符串中的特殊字符不会被转义
let str2 = "LIne 1 \nLine 2"//\n会被转义为换行符,打印是会分为两行

print(str1)
print(str2)

//初始化空字符串
var emptyString = ""
var emptyString1 = String();

print(emptyString)
print(emptyString1)
// 两个字符串均为空并等价。
print(emptyString1 == emptyString)

//字符串遍历
for character in "Dog!🐶" {
    print(character)
}

let char1: Character = "a";
print(char1)

//连接字符串
let string1 = "hello "
let string2 = "world!"

//通过 + 运算符拼接两个字符串
var string3 = string1 + string2;
print(string3)

//通过 += 将一个字符串添加到一个已经存在到字符串变量上
string3 += " swift";
print(string3)

//使用append()方法将一个字符添加到一个字符串变量的尾部
string3.append(" end")
print(string3)

let chicken = "\u{1F425}";
print(chicken)

//字符串索引
//greeting.startIndex字符串第一个字符的索引
//greeting.endIndex字符串最后一个字符串的索引+1
let greeting = "greeting"
for char in greeting[greeting.startIndex..<greeting.endIndex] {
    print(char);
}

//indices 字符串全部索引的范围
for index in greeting.indices {
    print("\(greeting[index]) ", terminator: "")
}
					
